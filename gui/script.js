'use strict';

let port;
let reader;
let inputDone;
let outputDone;
let inputStream;
let outputStream;

const N = 6;

const butConnect = document.getElementById('butConnect');
const divInfo = document.getElementById('info');

const divImage = document.getElementById('divImage');
const divData = document.getElementById('divData');

const bAdd = document.getElementById('bAdd');
const bClear = document.getElementById('bClear');
const rEmpty = document.getElementById('rEmpty');
const rSquare = document.getElementById('rSquare');
const rTriangle = document.getElementById('rTriangle');
const rCircle = document.getElementById('rCircle'); 

const divExportData = document.getElementById('divExportData');
const divStats = document.getElementById('divStats');

var intervalSerial;

var exportData = {"labels": [], "data": []};
var currentData = [];

document.addEventListener('DOMContentLoaded', () => {
  port = null;
  butConnect.addEventListener('click', clickConnect);
  bAdd.addEventListener('click', clickAdd);
  bClear.addEventListener('click', clickClear);

  if (!('serial' in navigator)) {
    setInfo(
      "Sorry, <b>Web Serial</b> is not supported on this device, make sure you're \
       running Chrome 78 or later and have enabled the \
       <code>#enable-experimental-web-platform-features</code> flag in \
       <code>chrome://flags</code>", "alert-danger");
   }
});

create_image();

async function connect() {
  port = await navigator.serial.requestPort();
  await port.open({ baudRate: 115200 });

  let decoder = new TextDecoderStream();
  inputDone = port.readable.pipeTo(decoder.writable);
  inputStream = decoder.readable
    .pipeThrough(new TransformStream(new LineBreakTransformer()));

  reader = inputStream.getReader();

  const encoder = new TextEncoderStream();
  outputDone = encoder.readable.pipeTo(port.writable);
  outputStream = encoder.writable;

  intervalSerial = window.setInterval(read_serial, 500);
}

async function disconnect() {
  if (reader) {
    await reader.cancel();
    await inputDone.catch(() => {});
    reader = null;
    inputDone = null;
  }
  if (outputStream) {
    await outputStream.getWriter().close();
    await outputDone;
    outputStream = null;
    outputDone = null;
  }
  await port.close();
  port = null;
  clearInterval(intervalSerial);
}

async function clickConnect() {
  if (port) {
    await disconnect();
    toggleUIConnected(false);
    return;
  }
  await connect();
  toggleUIConnected(true);
}

function writeToStream(...lines) {
  const writer = outputStream.getWriter();
  lines.forEach((line) => {
    console.log('[SEND]', line);
    writer.write(line + '\n');
  });
  writer.releaseLock();
}

function toggleUIConnected(connected) {
  let lbl = 'Connect';
  if (connected) {
    lbl = 'Disconnect';
    setInfo("Connected", "alert-success");
  } else {
    setInfo("Not connected", "alert-dark");
  }
  butConnect.textContent = lbl;
}

class LineBreakTransformer {
  constructor() {
    // A container for holding stream data until a new line.
    this.container = '';
  }

  transform(chunk, controller) {
    // CODELAB: Handle incoming chunk
    this.container += chunk;
    const lines = this.container.split('\n');
    this.container = lines.pop();
    lines.forEach(line => controller.enqueue(line));
  }

  flush(controller) {
    // CODELAB: Flush the stream.
    controller.enqueue(this.container);
  }
}

async function serial_send(cmd) {
  if (!port) {
    setInfo("Disconnected", "alert-warning");
    return;
  }
  setInfo("Sending: " + cmd, "alert-info");
  writeToStream(cmd);
}

async function read_serial() {
    const { value, done } = await reader.read();
    if (value) {
      console.log(value);
      divData.innerHTML = value;
      currentData = JSON.parse(value);
      for (var i=0; i<N; i++)
        for (var j=0; j<N; j++) {
          var a = document.getElementById("a_" + i + "_" + j);
          var d = currentData[i][j];
          var nd = 255 - d;
          a.innerHTML = d; 
          a.style.background = "rgb(" + nd + ", " + nd + ", " +  nd + ")";
          a.style.background = "rgb(" + d + ", " + d + ", " +  d + ")";
        } 
    }
    if (done) {
      console.log('[readLoop] DONE', done);
      reader.releaseLock();
    }
}

function setInfo(text, color) {
  divInfo.innerHTML = text;
  divInfo.className = "alert " + color;
}

async function i2c_read() {
  var t = "I " + I2CDevAddr.value + " " + I2CAddr.value;
  serial_send(t);
}

function to_ascii(val, len) {
  var txt = ""

  for (var j = 0; j < len; j++) {
    var n = parseInt("0x" + val.slice(2*(j+1), 2*(j+2)));
    txt += String.fromCharCode(n);
  }

  return txt;
}

function show_mem_info(elmnt) {
  memInfoH.innerHTML = elmnt.getAttribute('title');
  memInfoB.innerHTML = elmnt.getAttribute('data-bs-content');
}

function create_image() {
 var txt = "";

 for (var i = 0; i < N; i++) {
   for (var j = 0; j < N; j++) {
     txt += "<div id='a_" + i + "_" + j + "' class='image_px'></div>";
   }
   txt += "<div style='clear:both'></div>"
 }
 divImage.innerHTML = txt;
}

function clickAdd() {
  exportData["data"].push(currentData);
  exportData["labels"].push(document.querySelector('input[name="dataType"]:checked').value);
  showExportData();
}

function clickClear() {
  exportData["data"] = [];
  exportData["labels"] = [];
  showExportData();
}

function showExportData() {
  divExportData.innerHTML = JSON.stringify(exportData);
  var stats = "Empty: " + exportData["labels"].filter(x => x=="Empty").length + "</br>";
  stats += "Square: " + exportData["labels"].filter(x => x=="Square").length + "</br>";
  stats += "Triangle: " + exportData["labels"].filter(x => x=="Triangle").length + "</br>";
  stats += "Circle: " + exportData["labels"].filter(x => x=="Circle").length + "</br>";
  divStats.innerHTML = stats;
}


